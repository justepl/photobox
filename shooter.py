import subprocess
import os
import time
from io import BytesIO
from datetime import datetime
from PIL import Image
from os import path

# General settings
diskSpaceToReserve = 40 * 1024 * 1024 # keep 40mb free space on disk


# Var for fswebcam
destinationFolder = './snapshot/'
destinationFolderTest = './snapshot/test/'
width = 1280
height = 720
resolution = str(width) + 'x' + str(height)
testWidth = 128
testHeight = 72
testImgResolution = str(testWidth) + 'x' + str(testHeight)


# Motion detection settings:
# Threshold (how much a pixel has to change by to be marked as "changed")
# Sensitivity (how many changed pixels before capturing an image)
# ForceCapture (whether to force an image to be forced every forceCaptureTime seconds)
threshold = 10
sensitivity = (testWidth * testHeight)/25
print(sensitivity)
forceCapture = True
forceCaptureTime = 60 * 60 # Once a second


def snaptTestImage():
    time = datetime.now()
    filename = "capture-%04d%02d%02d-%02d%02d%02d.jpg" % (time.year, time.month, time.day, time.hour, time.minute, time.second)
    command = 'fswebcam -q -r ' + testImgResolution + ' --no-banner ' + destinationFolderTest + filename
    subprocess.call(command, shell=True)

    imageData = BytesIO()
    imageData.write(open(destinationFolderTest + filename, 'rb').read())
    imageData.seek(0)
    im = Image.open(imageData)
    buffer = im.load()
    imageData.close()
    os.remove(destinationFolderTest+filename)
    return im, buffer


def saveImage(width, height, diskSpaceToReserve):
    keepDiskSpaceFree(diskSpaceToReserve)
    time = datetime.now()
    filename = "capture-%04d%02d%02d-%02d%02d%02d.jpg" % (time.year, time.month, time.day, time.hour, time.minute, time.second)
    command = 'fswebcam -q -r ' + resolution + ' --no-banner ' + destinationFolder + filename
    subprocess.call(command, shell=True)
    print('Captured' + filename)

def getFreeSpace():
    st = os.statvfs("./snapshot")
    du = st.f_bavail * st.f_frsize
    return du

def keepDiskSpaceFree(bytesToReserve):
    if (getFreeSpace() < bytesToReserve):
        for filename in sorted(os.listdir("./snapshot/test")):
            if (filename.startswith('capture') and filename.endswith('.jpg')):
                os.remove(filename)
                if (getFreeSpace() > bytesToReserve):
                    return
        
        for filename in sorted(os.listdir('./snapshot')):
            if (filename.startswith('capture') and filename.endswith('.jpg')):
                os.remove(filename)
                if (getFreeSpace() > bytesToReserve):
                    return


# Prepare environment
if(not(path.exists('./snapshot'))):
    os.mkdir('./snapshot')
    os.mkdir('./snapshot/test')
if(not(path.exists('./snapshot/test'))):
    os.mkdir('./test')


#get first img:
image1, buffer1 = snaptTestImage()

lastCapture = time.time()

while(True):
    #get second img
    image2, buffer2 = snaptTestImage()

    #count changed Pixels
    changedPixels = 0
    for x in range(0, testWidth):
        for y in range(0, testHeight):
            # Just check green channel as it's the highest quality channel
            pixdiff = abs(buffer1[x,y][1] - buffer2[x,y][1])
            if (pixdiff > threshold):
                changedPixels +=1

    print(changedPixels)
    # Check forceCapture
    if (forceCapture):
        if (time.time() - lastCapture > forceCaptureTime):
            changedPixels = sensitivity + 1
        
    # Save an image if many pixels have changed
    if (changedPixels > sensitivity):
        print("PHOTO MAMEN")
        lastCapture = time.time()
        saveImage(width, height, diskSpaceToReserve)

    # swap comparison image and buffers
    image1 = image2
    buffer1 = buffer2